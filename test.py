import random

from obfuscate_mailto import encrypt_mail


TESTKEY = 'poixqkwzlygvnsxpo'
TESTDATA = [
    ('mvrekous', 'fc*n`U%]__U\]"]V]RYVma&eq__Sf', ''),
    ('hilboll', 'V^v]WYYdYnW1W]eS_]\*n`U%]__U', ''),
    ('daskalakis', '`*NZeWYgNeQj/igZ{SbOfWZ&_RgI`[h', ''),
]
ORIGIN = 33
SIZE = 127 - 33


def test_encrypt_mail():
    for email, crypt, _ in TESTDATA:
        actual = encrypt_mail(
            'mailto:{}@uni-bremen.de'.format(email), ORIGIN, SIZE, TESTKEY)
        assert actual == crypt
